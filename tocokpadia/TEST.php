<?php require '../connect.php'; 
   session_start();
   $sql = $db->prepare("SELECT * FROM `keranjang`");
   $sql->execute();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tocokpedia</title>
    <link rel="icon" href ="https://4.bp.blogspot.com/-ItRaVmM-PoU/XgrlppcnvcI/AAAAAAAABPY/Pbgwlu9Gb7UKLJFekuqk5__OPWQvqq08gCLcBGAsYHQ/s200-c/shopee%2B1.png" type="image/x-icon">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</head>
<body>
<!-- ------Navbar Buka------- -->
<nav class="navbar navbar-expand-lg navbar-light bg-success">
    <a class="navbar-brand text-light" href="../index.php">Tocokpedia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link text-light" href="/keranjang/index.php">Keranjang Anda</a>
            </li>
            <li class="nav-item">
                <a type="button" class="nav-link text-light" data-toggle="modal" data-target="#exampleModal">Login</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cek Harga
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Harga Murah</a>
                <a class="dropdown-item" href="#">Harga Trending</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="tambah_jualan/index.php">Tambah barang</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<!-- -----NAVBAR TUTUP----- -->
<div class="card text-dark border-success mb-3" id="boxCard" style="max-width: 18rem;">

<!-- ---------Nama Pengguna Keranjang---------- -->
<div class="card-header" style="font-size: 25px">

<?php require_once '../connect.php';
    if (!empty($_SESSION['username'])) {
        ?>
        Daftar Belanjaan <?= $_SESSION['username'] ?>
    <?php
        }
    ?>

</div>
<!-- ------------Nama Pengguna Keranjang(CLOSED)----------> 


<!-- ----------PANGGIL DATABASE------------ -->
    <?php
        if (empty($_SESSION['username'])) {
            echo 'Maaf anda belum login';
        }else{
        $uid = $_SESSION['username'];
        $sql = $db->prepare("SELECT * FROM keranjang WHERE user_id = '$uid'");
        $sql->execute();
                
        while ($fetch = $sql->fetch()) {
    ?>
<!-- ----------Panggil Database(CLOSED)---------- -->

<div class="card text-dark mb-1" style="max-width: 60rem;" id="cardStyle">
    <div class="row no-gutters">
        <div class="col-md-4">
        <img class="cardImg" src="../gambar/<?php echo $fetch['image_name'] ?>">
    </div>
    <div class="col-md-8">
        <div class="card-body">
        <h5 class="card-title"><?= $fetch['barang'] ?></h5>
        <p class="card-text">Harga : <?= $fetch['harga_barang']?></p>
        <p class="card-text">Jumlah : </p>     
        </div>
        </div>
    </div>
</div>
<!-- ---------PENUTUP PHP----------- -->
    <?php
        }
    ?>
    <?php
        }
    ?>
<!-- ----------PENUTUP PHP(CLOSED)---------- -->
</div>
</body>
</html>