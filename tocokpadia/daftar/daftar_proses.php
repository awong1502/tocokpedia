<?php require '../connect.php';

if (!empty($_POST)) {
    $uname = $_POST['username'];
    $pword = $_POST['password'];

    $insert_query = $db->prepare("INSERT INTO user (username, password) VALUES(?, ?)");
    $insert_query->execute([
        $uname, $pword
    ]);
    header ('location: ../index.php');
}
