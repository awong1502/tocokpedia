<?php require '../connect.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login Page</title>
    <link rel="icon" href ="https://4.bp.blogspot.com/-ItRaVmM-PoU/XgrlppcnvcI/AAAAAAAABPY/Pbgwlu9Gb7UKLJFekuqk5__OPWQvqq08gCLcBGAsYHQ/s200-c/shopee%2B1.png" type="image/x-icon">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
</head>
<body>
<!-- ------Navbar Buka------- -->
<nav class="navbar navbar-expand-lg navbar-light bg-success">
    <a class="navbar-brand text-light" href="../index.php">Tocokpadia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link text-light" href="/keranjang/index.php">Keranjang Anda</a>
            </li>
            <li class="nav-item">
                <a type="button" class="nav-link text-light" data-toggle="modal" data-target="#exampleModal">Login</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cek Harga
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Harga Murah</a>
                <a class="dropdown-item" href="#">Harga Trending</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="tambah_jualan/index.php">Tambah barang</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<!-- -----NAVBAR TUTUP----- -->

<!-- ------LOGIN-------- -->
<div class="card text-center" id="login">
  <div class="card-header">
    <ul class="nav nav-pills card-header-pills">
      <li class="nav-item">
        <a class="nav-link" style="color: green;" href="../login/index.php">Sign In</a>
      </li>
      <li class="nav-item">
        <a class="btn btn-success">Sign Up</a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <h5 class="card-title">Silahkan Daftar terlebih dahulu !!</h5>
    <form method="post" action="daftar_proses.php">
        <label for="">Username</label></br>
        <input type="text" name="username" id="username" ></br></br>
        <label for="">Password</label></br>
        <input type="password" name="password" id="password" ></br></br>
        <button type="input" class="btn btn-success">Ayo bergabung dengan kami</button>
    </form>
  </div>
</div>
</body>
</html>