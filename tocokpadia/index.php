<?php require 'connect.php';
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Tocokpadia</title>
    <link rel="icon" href ="https://4.bp.blogspot.com/-ItRaVmM-PoU/XgrlppcnvcI/AAAAAAAABPY/Pbgwlu9Gb7UKLJFekuqk5__OPWQvqq08gCLcBGAsYHQ/s200-c/shopee%2B1.png" type="image/x-icon">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</head>
<body>
<!-- ----------MODAL----------- -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-success text-light">
        <h5 class="modal-title" id="exampleModalLabel">Sign In Page</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

        <div class="modal-body">
      
            <div class="card text-center" id="login">
                    <ul class="nav card-header bg-success">
                    <li class="nav-item">
                        <a class="btn btn-light bg-light text-primary" href="index.php">Sign In</a>
                    </li>
                <li class="nav-item">
                    <a class="nav-link text-light" href="daftar/index.php">Sign Up</a>
                </li>
                    </ul>
            </div>
            <div class="card-body">
                <h5 class="card-title">Silahkan Login terlebih dahulu !!</h5>
                <form action="Other/login_proses.php" method="post" >
                    <label for="">Username</label></br>
                    <input type="text" name="username" id="username" class="form-control"></br></br>
                    <label for="">Password</label></br>
                    <input type="password" name="password" id="password" class="form-control"></br></br>
                    <button type="input" name="login-btn" value="login" class="btn btn-light bg-light text-primary">Masuk</button>
                </form>
            </div>
                
      </div>
    </div>
  </div>
</div>
<!-- -------------MODAL TUTUP-------------------- -->

<!-- ------Navbar Buka------- -->
<nav class="navbar navbar-expand-lg navbar-light bg-success">
    <a class="navbar-brand text-light" href="index.php">Tocokpadia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link text-light" href="keranjang/index.php">Keranjang Anda</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cek Harga
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Harga Murah</a>
                <a class="dropdown-item" href="#">Harga Trending</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="tambah_jualan/index.php">Tambah barang</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <?php require_once 'connect.php';
            if(!empty($_SESSION['username'])) {
        ?>
            <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <?= $_SESSION['username'] ?>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="Other/logout.php" class="btn btn-success">LOGOUT</a>
                </div>
            </li>
            </ul>
        <?php
            }else{
        ?>
            <a type="button" class="nav-link text-light" data-toggle="modal" data-target="#exampleModal">Login</a>
        
        <?php
            }
        ?>
    </div>
    </form>
</nav>
<!-- -----NAVBAR TUTUP----- -->

<!-- -------ITEM CONTENT--------- -->
<section class="container" id="content-1">
<?php
    require_once 'connect.php';
                    
    $sql = $db->prepare("SELECT * FROM `barang_jual`");
    $sql->execute();
                                    
    while ($fetch = $sql->fetch()) {
    ?>
    
    <div class="card border-success" id="cardDua" style="width: 18rem;">
        <div class="card-header bg-transparent border-success text-success" id="cardTitle">
            <h5 class="card-title" style="padding-bottom: 1rem;"><?= $fetch['barang'] ?></h5>
        </div>
        <img src="gambar/<?php echo $fetch['image_name'] ?>" class="card-img-top" alt="">
            <div class="border rounded-sm border-success" id="descImg">
                <p class="card-text"><?= $fetch['desc_barang'] ?></p> 
            </div>
        <div class="card-footer bg-transparent border-success" id="footerImg">
            <p class="card-text font-weight-bold" id="harga">Harga : <?= $fetch['harga_barang'] ?></p>

            <form class="form" method="post" id="formId" action="Other/keranjang_proses.php?id=<?php echo $fetch['id'] ;?>&namabarang=<?php echo $fetch['barang'] ?>">
                <input class="form-control" id="inputForm" type="number" name="jumlah" placeholder="Jumlah barang anda">
                <button class="btn btn-success" id="buttonForm">Masukkan ke keranjang</button>
            </form>
        </div>
    </div>

    <?php
        }
    ?>   




</section>
</body>
</html>