<?php require '../connect.php';

session_start();

if(!isset($_POST['username'])) {
    header("location: ../index.php");
}

if(isset($_POST['username'])) {
    $uname = $_POST['username'];
    $pword = $_POST['password'];

    try {
        $sql = "SELECT * FROM user WHERE username = :username AND password = :password";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('username', $uname);
        $stmt->bindParam('password', $pword);
        $stmt->execute();

        $count = $stmt->rowCount();
        if($count == 1) {
            $_SESSION['username'] = $uname;
            header("location: ../index.php?user_id=$uname");
            return;
        }else{
            echo "Anda tidak dapat login";
        }
    }
    catch(PDOException $e) {
        echo $e->getMessage();
    }
}