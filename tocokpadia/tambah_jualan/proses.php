<?php	
	require_once '../connect.php';
	
	if(ISSET($_POST['upload'])){
		$barang = $_POST['barang'];
        $harga = $_POST['harga'];
        $desc = $_POST['desc'];
		$file_name = $_FILES['image']['name'];
		$file_temp = $_FILES['image']['tmp_name'];
		$allowed_ext = array("jpg", "jpeg", "gif", "png");
		$exp = explode(".", $file_name);
		$ext = end($exp);
		$path = "../gambar/".$file_name;
		if(in_array($ext, $allowed_ext)){
			if(move_uploaded_file($file_temp, $path)){
				try{
					$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$insert_query = $db->prepare("INSERT INTO `barang_jual`(`barang`, `harga_barang`, `desc_barang`, `image_name`, `location`) VALUES (?,?,?,?, ?)");
					$insert_query->execute([
						$barang, $harga, $desc, $file_name, $path
					]);
					}catch(PDOException $e){
						echo $e->getMessage();
					}
					
					$conn = null;
					header('location: ../index.php');
				header('location: ../index.php');
			
		}
	}
	}


?>