<?php require '../connect.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tocokpedia</title>
    <link rel="icon" href ="https://4.bp.blogspot.com/-ItRaVmM-PoU/XgrlppcnvcI/AAAAAAAABPY/Pbgwlu9Gb7UKLJFekuqk5__OPWQvqq08gCLcBGAsYHQ/s200-c/shopee%2B1.png" type="image/x-icon">
    <link rel="stylesheet" href="index.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<body>
<!-- ------Navbar Buka------- -->
<nav class="navbar navbar-expand-lg navbar-light bg-success">
    <a class="navbar-brand text-light" href="../index.php">Tocokpadia</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link text-light" href="/keranjang/index.php">Keranjang Anda</a>
            </li>
            <li class="nav-item">
                <a type="button" class="nav-link text-light" data-toggle="modal" data-target="#exampleModal">Login</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cek Harga
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Harga Murah</a>
                <a class="dropdown-item" href="#">Harga Trending</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Tambah barang</a>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>
<!-- -----NAVBAR TUTUP----- -->


<div class="container" id="cont-1">

    <div class="card" id="sideNavbar" style="width: 18rem;">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">Item</li>
        <li class="list-group-item">Jual Baru</li>
        <li class="list-group-item">Jual Bekas</li>
    </ul>
    </div>

    <form method="POST" enctype="multipart/form-data" action="proses.php" class="form col-md-6 border border-success rounded" id="form">
        <label>Nama Barang</label>
        <input name="barang" type="text" class="form-control" required="required" placeholder="Masukan Nama Barang Anda">
        <label>Harga</label>
        <input name="harga" type="text" class="form-control" required="required" placeholder="Masukkan Harga">
        <label>Deskripsi</label>
        <input name="desc" type="text" class="form-control" required="required" placeholder="Isi Deskripsi">
        <label>Gambar</label>
        <input name="image" type="file" class="form-control" required="required">
        <button class="btn btn-primary" name="upload">Unggah</button></br></br>
    </form>
   
</div>

</body>
</html>